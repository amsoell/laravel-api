## Laravel API

This is an example of a RESTful API developed in PHP on the [Laravel](http://github.com/laravel/laravel) framework, specifically [version 4.2](https://github.com/laravel/laravel/releases/tag/v4.2.0). It uses a MySQL database on the backend and includes seeders to populating the initial user table with 5 *manager* users and 25 *employee* users.

The application includes unit tests for controllers as well as API endpoints built on Laravel's PHPUnit extensions. Tests can be run by running `phpunit` from the project root.

## Online demo
This project is also available for evaluation at [http://amsoell.com/api/public/v1.1](http://amsoell.com/api/public/v1.1). The main index page includes a test suite for evaluating the various API endpoints. If you're interested in seeing a front-end application that leverages this API written in Ember.js? It's hosted on [Heroku](http://amsoell-employee-scheduler.herokuapp.com) with code available on [GitHub](http://github.com/amsoell/employee-scheduler);

### Endpoints
The following endpoints have been implemented:

`/v1.1/users`  
GET—Get a full listing of users  
POST—Create a new user  

`/v1.1/users/{users}`   
GET—Get details on user with ID _{users}_  
PUT—Update details for user with ID _{users}_  
DELETE—Delete user with ID _{users}_  

`/v1.1/shifts`  
GET—Get a full listing of shifts  
POST—Create a new shift  

`/v1.1/shifts/{shifts}`  
GET—Get details on shift with ID _{users}_  
PUT—Update details for shift with ID _{users}_  
DELETE—Delete shift with ID _{users}_  

### Authentication
Authentication is implemented with Basic Authentication. All seeded users are built with the default passwords **EmployeePassword** or **ManagerPassword**, but new users can also be created through the `/v1.1/users` endpoint.

### Feature Set

The project specifications include the ability to infer the following information. Completed items are indicated as such:

- [ ] As an employee, I want to know when I am working, by being able to see all of the shifts assigned to me.
- [ ] As an employee, I want to know who I am working with, by being able see the employees that are working during the same time period as me.
- [ ] As an employee, I want to know how much I worked, by being able to get a summary of hours worked for each week.
- [x] As an employee, I want to be able to contact my managers, by seeing manager contact information for my shifts.

- [x] As a manager, I want to schedule my employees, by creating shifts for any employee.
- [ ] As a manager, I want to see the schedule, by listing shifts within a specific time period.
- [x] As a manager, I want to be able to change a shift, by updating the time details.
- [x] As a manager, I want to be able to assign a shift, by changing the employee that will work a shift.
- [x] As a manager, I want to contact an employee, by seeing employee details.

### Todo

Upcoming feature enhancements

- [ ] Add API endpoints to fully satisfy the [Feature Set](#feature-set) requirements
- [ ] OAuth 2.0 authentication. 
- [x] Flesh out the error messages. As of version 1.0, API calls that are unsuccessful simply return `status=error` without much explanation as to why the call failed (_Update:_ Version 1.1 errors now include the `messages` property as an array of errors returned from the `Validator` class)
- [x] Obviously if this were to be a full-featured API, there would be a lot more calls implemented, such as employees requesting shift changes, deleting shifts, changing user roles, etc. (_Update:_ Version 1.1 includes proper, basic RESTful API endpoints. More are needed to satisfy the [Feature Set](#feature-set) requirements, but basic CRUD endpoints are there).

### Version 1.0

For posterity, here's a link the original [Version 1.0 commit](https://github.com/amsoell/laravel-api/tree/cf16504c7208d77c2f5e1b42180011429b2391f6) including the accompanying [README.md](https://github.com/amsoell/laravel-api/blob/cf16504c7208d77c2f5e1b42180011429b2391f6/README.md).
