<?php

class ShiftsController extends BaseController {

  public $restful = true;

  //! Create a new shift
  // @params
  //  start_time
  //  end_time
  //  manager_id
  //  employee_id (optional)
  public function create() {

    $validator = Validator::make(
      Input::all(),
      array(
        'start_time'    => 'date_format:Y-m-d H:i:s|required',
        'end_time'      => 'date_format:Y-m-d H:i:s|required',
        'manager_id'    => 'integer|required|min:1',
        'employee_id'   => 'integer'
      )
    );

    if ($validator->passes()) {
      $shift = new Shifts;
      $shift->start_time  = Input::get('start_time');
      $shift->end_time    = Input::get('end_time');
      $shift->manager_id  = Input::get('manager_id');
      if (strlen(Input::get('employee_id'))>0) $shift->employee_id = Input::get('employee_id');
      if (strlen(Input::get('break'))>0) $shift->break = Input::get('break');
      $shift->save();

      $shift->status = "ok";
      return $shift;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }

  }

  //! Update existing shift details
  public function update($id = null) {
    if (is_null($id)) {
      // V1 method
      $validator = Validator::make(
        Input::all(),
        array(
          'start_time'    => 'date_format:Y-m-d H:i:s|required',
          'end_time'      => 'date_format:Y-m-d H:i:s|required',
          'shift_id'      => 'integer|required|min:1'
        )
      );

      if ($validator->passes()) {
        $shift = Shifts::find(Input::get('shift_id'));
        $shift->start_time = Input::get('start_time');
        $shift->end_time   = Input::get('end_time');
        $shift->save();

        $shift->status = "ok";
        return $shift;
      } else {
        $response             = new stdClass();
        $response->status     = "error";
        $response->message    = "validation";

        return json_encode($response);
      }
    } else {
      // V1.1 method
      $input = Input::all();
      $input['id'] = $id;
      $validator = Validator::make(
        $input,
        array(
          'id'            => 'integer|required|min:1',
          'start_time'    => 'date_format:Y-m-d H:i:s',
          'end_time'      => 'date_format:Y-m-d H:i:s',
          'employee_id'   => 'integer|min:1',
          'manager_id'    => 'integer|min:1',
          'break'         => 'regex:/^[0-9.]*$/'
        )
      );

      if ($validator->passes()) {
        $ret = new stdClass();
        $ret->status = 'ok';

        $shift = Shifts::find($id);
        if (Input::has('start_time'))  $shift->start_time  = Input::get('start_time');
        if (Input::has('end_time'))    $shift->end_time    = Input::get('end_time');
        if (Input::has('employee_id')) $shift->employee_id = Input::get('employee_id');
        if (Input::has('manager_id'))  $shift->manager_id  = Input::get('manager_id');
        if (Input::has('break'))       $shift->break       = Input::get('break');
        $shift->save();

        $ret->shift = $shift;

        return json_encode($ret);
      } else {
        $response             = new stdClass();
        $messages             = $validator->messages();
        $response->status     = "error";
        $response->type       = "validation";
        $response->messages   = $messages->all();

        return json_encode($response);
      }

    }
  }

  //! Get employees scheduled for a shift
  public function getEmployees() {
    $validator = Validator::make(
      Input::all(),
      array(
        'shift_id'      => 'integer|required|min:1'
      )
    );

    if ($validator->passes()) {
      $shift = Shifts::find(Input::get('shift_id'));
      $employees = Users::where('id', $shift->employee_id)->get();

      $employees->status = "ok";
      return $employees;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }
  }

  //! Get managers scheduled for a shift
  public function getManagers() {
    $validator = Validator::make(
      Input::all(),
      array(
        'shift_id'      => 'integer|required|min:1'
      )
    );

    if ($validator->passes()) {
      $shift = Shifts::find(Input::get('shift_id'));
      $manager = Employees::where('id', $shift->manager_id)->get();

      $manager->status = "ok";
      return $manager;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }
  }

  //! Get shifts within a given date range
  public function getSchedule() {
    $validator = Validator::make(
      Input::all(),
      array(
        'start_time'    => 'date_format:Y-m-d|required',
        'end_time'      => 'date_format:Y-m-d|required'
      )
    );

    if ($validator->passes()) {
      // Compare the entered start date to the shift's end date (and conversely the entered end date with the shift's start date) to make sure we catch shifts that span the start/end dates
      $shifts = Shifts::where('end_time', '>=', Input::get('start_time'))
                  ->where('start_time', '<=', Input::get('end_time'))
                  ->orderBy('start_time')
                  ->get();

      for ($i=0; $i<count($shifts); $i++) {
        $shifts[$i]->employee = Users::where('id', $shifts[$i]->employee_id)->get();
        $shifts[$i]->manager = Users::where('id', $shifts[$i]->manager_id)->get();
        unset($shifts[$i]->manager_id, $shifts[$i]->employee_id);
      }

      $shifts->status = "ok";
      return $shifts;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }
  }

  //! Get shifts that overlap with an inputted shift
  public function getOverlappingShifts() {
    $validator = Validator::make(
      Input::all(),
      array(
        'shift_id'      => 'integer|required|min:1'
      )
    );

    if ($validator->passes()) {
      $shift = Shifts::find(Input::get('shift_id'));

      // How do we tell if it's overlapping?:
      //  1) start_date occurs before targetted end_date, AND
      //  2) end_date occurs after targetting start_date
      // This covers all four scenarios of overlapping shifts (starts before, ends during; starts during, ends after; starts during, ends during; starts before, ends after
      $overlappingShifts = Shifts::where('start_time','<=',$shift->end_time)
                            ->where('end_time','>=', $shift->start_time)
                            ->where('id','!=', $shift->id)
                            ->orderBy('start_time')
                            ->get();

      for ($i=0; $i<count($overlappingShifts); $i++) {
        $overlappingShifts[$i]->employee = Users::where('id', $overlappingShifts[$i]->employee_id)->get();
        $overlappingShifts[$i]->manager = Users::where('id', $overlappingShifts[$i]->manager_id)->get();
        unset($overlappingShifts[$i]->manager_id, $overlappingShifts[$i]->employee_id);
      }

      $overlappingShifts->status = "ok";
      return $overlappingShifts;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }
  }

  //! Assign an employee to a shift
  public function assign() {
    $validator = Validator::make(
      Input::all(),
      array(
        'employee_id'   => 'integer|required|min:1',
        'shift_id'      => 'integer|required|min:1'
      )
    );

    if ($validator->passes()) {
      $shift = Shifts::find(Input::get('shift_id'));
      $shift->employee_id = Input::get('employee_id');
      $shift->save();

      $shift->status = "ok";
      return $shift;
    } else {
      $response             = new stdClass();
      $response->status     = "error";
      $response->message    = "validation";

      return json_encode($response);
    }
  }

  //! V1.1 METHODS

  public function index($id = null) {
    $validator = Validator::make(
      array('id' => $id),
      array('id' => 'integer|min:1')
    );

    if ($validator->passes()) {
      if (isset($id) && is_numeric($id)) {
        // Return a shift's details
        $ret = new stdClass();
        $ret->status = "ok";
        $ret->shift = Shifts::find($id);

        return json_encode($ret);
      } else {
        // Return all shifts
        $sortBy     = (Request::query('sortBy')?Request::query('sortBy'):'start_time');
        $sortDir    = (Request::query('sortAscending')=='false'?'DESC':'ASC');

        $ret = new stdClass();
        $ret->status = "ok";
        $ret->shifts = Shifts::orderBy($sortBy, $sortDir)->get();

        return json_encode($ret);
      }
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  public function show($id) {
    return $this->index($id);
  }

  public function store() {
    $ret = new stdClass();
    $shift = $this->create();
    $ret->status = $shift->status;
    $ret->shift = $shift;
    unset($ret->shift->status);

    return json_encode($ret);
  }

  public function destroy($id) {
    $validator = Validator::make(
      array('id' => $id),
      array('id' => 'integer|min:1')
    );

    if ($validator->passes()) {
      $ret = new stdClass();
      $ret->status = "ok";
      $shift = Shifts::destroy($id);

      return json_encode($ret);
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

}
