<?php

class UsersController extends BaseController {

  public $restful = true;

  //! Create a user
  public function create() {
    $validator = Validator::make(
      Input::all(),
      array(
        'name'      => 'required|max:100',
        'role'      => 'in:manager,employee|required',
        'email'     => 'email|unique:users|max:100|required_without:phone',
        'phone'     => 'unique:users|max:100|required_without:email',
        'password'  => 'required'
      )
    );

    if ($validator->passes()) {
      $ret = new stdClass();

      $user           = new Users;
      $user->name     = Input::get('name');
      $user->role     = Input::get('role');
      $user->email    = Input::get('email');
      $user->phone    = Input::get('phone');
      $user->password = Hash::make(Input::get('password'));
      $user->save();

      $ret->status = "ok";
      $ret->user = $user;
      return json_encode($ret);
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }

  }

  //! Get an employee's upcoming shifts
  public function getSchedule() {
    $validator = Validator::make(
      Input::all(),
      array(
        'employee_id' => 'required|integer|min:1'
      )
    );

    if ($validator->passes()) {
      // Query based on the end date, to ensure we capture any shifts we're that are in progress
      $scheduleData = Shifts::where('employee_id', Input::get('employee_id'))
                        ->where('end_time', '>=', DB::raw('NOW()'))
                        ->get();

      for ($i=0; $i<count($scheduleData); $i++) {
        $scheduleData[$i]->manager = Users::where('id',$scheduleData[$i]->manager_id)->get();
        unset($scheduleData[$i]->manager_id);
      }

      $scheduleData->status = "ok";
      return $scheduleData;
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  //! Get sum of hours worked over the past 12 weeks
  public function getTotalHours() {
    $validator = Validator::make(
      Input::all(),
      array(
        'employee_id' => 'required|integer|min:1'
      )
    );

    if ($validator->passes()) {
      // Raw SQL query: SELECT id, WEEKOFYEAR(start_time) AS Week, SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time)) / 3600) AS TotalWorkedInPeriod FROM shifts WHERE employee_id=5 GROUP BY WEEKOFYEAR(start_time)
      $shiftData = Shifts::where('employee_id', Input::get('employee_id'))
                      ->groupBy(DB::raw('WEEKOFYEAR(start_time)'))
                      ->get(array('id', DB::raw('WEEKOFYEAR(start_time) AS Week'), DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time))/3600) AS TotalWorkedInPeriod')));

      $shiftData->status = "ok";
      return $shiftData;
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  //! Get an employee's details
  public function getDetails() {
    $validator = Validator::make(
      Input::all(),
      array(
        'employee_id' => 'required|integer|min:1'
      )
    );

    if ($validator->passes()) {
      $user = Users::where('id', Input::get('employee_id'))->get();
      $user->status = "ok";
      return $user;
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  //! V1.1 METHODS

  public function index($id = null) {
    $validator = Validator::make(
      array('id' => $id),
      array('id' => 'integer|min:1')
    );

    if ($validator->passes()) {
      if (isset($id) && is_numeric($id)) {
        // Return a user's details
        $ret = new stdClass();
        $ret->status = "ok";
        $ret->user = Users::find($id);

        return json_encode($ret);
      } else {
        // Return all users
        $sortBy     = (Request::query('sortBy')?Request::query('sortBy'):'name');
        $sortDir    = (Request::query('sortAscending')=='false'?'DESC':'ASC');

        $ret = new stdClass();
        $ret->status = "ok";
        $ret->users = Users::orderBy($sortBy, $sortDir)->get();

        return json_encode($ret);
      }
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  public function show($id) {
    return $this->index($id);
  }

  public function store() {
    return $this->create();
  }

  public function update($id) {
    $validator = Validator::make(
      Input::all(),
      array(
        'name'      => 'max:100',
        'role'      => 'in:manager,employee',
        'email'     => 'email|unique:users|max:100',
        'phone'     => 'unique:users|max:100'
      )
    );

    if ($validator->passes()) {
      $ret = new stdClass();
      $ret->status = 'ok';

      $user = Users::find($id);
      if (Input::has('name'))      $user->name      = Input::get('name');
      if (Input::has('role'))      $user->role      = Input::get('role');
      if (Input::has('email'))     $user->email     = Input::get('email');
      if (Input::has('phone'))     $user->phone     = Input::get('phone');
      if (Input::has('password'))  $user->password  = Hash::make(Input::get('password'));
      $user->save();

      $ret->user = $user;

      return json_encode($ret);
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

  public function destroy($id) {
    $validator = Validator::make(
      array('id' => $id),
      array('id' => 'integer|min:1')
    );

    if ($validator->passes()) {
      $ret = new stdClass();
      $ret->status = "ok";
      Users::destroy($id);

      return json_encode($ret);
    } else {
      $r             = new stdClass();
      $messages      = $validator->messages();
      $r->status     = "error";
      $r->type       = "validation";
      $r->messages   = $messages->all();

      return json_encode($r);
    }
  }

}
