<?php

  use Illuminate\Auth\UserTrait;
  use Illuminate\Auth\UserInterface;
  use Illuminate\Auth\Reminders\RemindableTrait;
  use Illuminate\Auth\Reminders\RemindableInterface;

  class Users extends Eloquent implements UserInterface, RemindableInterface {

    public function getAuthIdentifier() {
        return $this->email;
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
      return $this->remember_token;
    }

    public function setRememberToken($value) {
      $this->remember_token = $value;
    }

    public function getRememberTokenName() {
      return "remember_token";
    }

    public function getReminderEmail() {
      return $this->email;
    }

    protected $hidden = array('password', 'remember_token', 'created_at', 'updated_at');

  }

?>