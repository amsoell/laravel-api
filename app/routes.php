<?php

//! VERSION 1.1
Route::group(['prefix' => 'v1.1'], function() {
  Route::get("/", function() {
    return View::make('UserStories-v1_1');
  });

  Route::group(['before' => 'auth.basic'], function() {
    Route::resource("shifts",'ShiftsController');
    Route::resource("users", 'UsersController');
  });
});

//! VERSION 1 - Keeping it around to remember how far we've come
Route::group(['prefix' => 'v1'], function() {
  Route::get("/", function() {
    return View::make('UserStories');
  });

  Route::group(['before' => 'auth.basic'], function() {
    Route::get ("getSchedule",            'UsersController@getSchedule');
    Route::get ("getOverlappingShifts",   'ShiftsController@getOverlappingShifts');
    Route::get ("getEmployeeHoursWorked", 'UsersController@getTotalHours');
    Route::post("createShift",            'ShiftsController@create');
    Route::get ("getUpcomingShifts",      'ShiftsController@getSchedule');
    Route::put ("updateShift",            'ShiftsController@store');
    Route::put ("assignShift",            'ShiftsController@assign');
    Route::get ("getEmployee",            'UsersController@getDetails');
    Route::post("createUser",             'UsersController@create');
  });
});

