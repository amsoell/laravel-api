<?php

class ShiftsApiEndpointTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->instance = new ShiftsController();
  }

  public function testGetAllShiftData_ReturnsStatusOK() {
    $response = $this->call('GET', 'v1.1/shifts');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testGetAllShiftData_ReturnsArrayOfShifts() {
    $response = $this->call('GET', 'v1.1/shifts');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertObjectHasAttribute('shifts', $data);
    $this->assertInternalType('array', $data->shifts);
  }

  public function testGetAllShiftData_ReturnsValidShiftData() {
    $response = $this->call('GET', 'v1.1/shifts');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());

    $this->assertGreaterThan(0, count($data->shifts));

    $this->assertEquals('2016-08-01 10:00:00',  $data->shifts[0]->start_time);
    $this->assertEquals('2016-08-01 16:00:00',  $data->shifts[0]->end_time);
    $this->assertEquals(1,                      $data->shifts[0]->manager_id);
    $this->assertEquals(3,                      $data->shifts[0]->employee_id);
    $this->assertEquals(0.5,                    $data->shifts[0]->break);
  }


  public function testCreateShift_ReturnsStatusOK() {
    $input = [
      'start_time'    => '2016-08-03 10:00:00',
      'end_time'      => '2016-08-03 16:00:00',
      'manager_id'    => 1,
      'employee_id'   => 4,
      'break'         => 0.5
    ];

    $response = $this->call('POST', 'v1.1/shifts', $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testCreateShift_DoesCreateShiftRecord() {
    $input = [
      'start_time'    => '2016-10-01 16:00:00',
      'end_time'      => '2016-10-01 23:30:00',
      'manager_id'    => 2,
      'employee_id'   => 4,
      'break'         => 1.0
    ];

    $response = $this->call('POST', 'v1.1/shifts', $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
    $this->assertObjectHasAttribute('shift', $data);
    $this->assertObjectHasAttribute('id', $data->shift);
    $shift = Shifts::find($data->shift->id);

    $this->assertEquals($input['start_time'],   $shift->start_time);
    $this->assertEquals($input['end_time'],     $shift->end_time);
    $this->assertEquals($input['manager_id'],   $shift->manager_id);
    $this->assertEquals($input['employee_id'],  $shift->employee_id);
    $this->assertEquals($input['break'],        $shift->break);
  }


  public function testUpdateShift_ReturnsStatusOK() {
    $input = [
      'start_time'    => '2016-08-03 10:00:00',
      'end_time'      => '2016-08-03 16:00:00',
      'employee_id'   => 2,
      'manager_id'    => 3,
      'break'         => 1.0
    ];

    $response = $this->call('PUT', "v1.1/shifts/1", $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testUpdateShift_DoesUpdateShiftRecord() {
    $input = [
      'start_time'    => '2016-08-03 11:00:00',
      'end_time'      => '2016-08-03 17:00:00',
      'employee_id'   => 1,
      'manager_id'    => 4,
      'break'         => 1.5
    ];

    $response = $this->call('PUT', "v1.1/shifts/3", $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
    $this->assertObjectHasAttribute('shift', $data);
    $this->assertObjectHasAttribute('id', $data->shift);
    $shift = Shifts::find($data->shift->id);

    $this->assertEquals($input['start_time'],   $shift->start_time);
    $this->assertEquals($input['end_time'],     $shift->end_time);
    $this->assertEquals($input['employee_id'],  $shift->employee_id);
    $this->assertEquals($input['manager_id'],   $shift->manager_id);
    $this->assertEquals($input['break'],        $shift->break);
  }

  public function testGetSingleShift_ReturnsStatusOK() {
    $response = $this->call('GET', "v1.1/shifts/2");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testGetSingleShift_ReturnsShift() {
    $response = $this->call('GET', "v1.1/shifts/2");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());

    $this->assertObjectHasAttribute('shift',    $data);
    $this->assertEquals('2016-08-01 16:00:00',  $data->shift->start_time);
    $this->assertEquals('2016-08-01 23:30:00',  $data->shift->end_time);
    $this->assertEquals(2,                      $data->shift->manager_id);
    $this->assertEquals(4,                      $data->shift->employee_id);
    $this->assertEquals(0.5,                    $data->shift->break);
  }

  public function testDeleteSingleShift_ReturnsStatusOK() {
    $response = $this->call('DELETE', "v1.1/shifts/1");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testDeleteSingleShift_DoesDeleteShift() {
    $data = Shifts::find(1);
    $this->assertNotNull($data);

    $response = $this->call('DELETE', "v1.1/shifts/1");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());

    $data = Shifts::find(1);
    $this->assertNull($data);
  }
}
