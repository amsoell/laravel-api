<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

  public function setUp() {
      parent::setUp();

      $this->prepareForTests();
  }

	public function createApplication()	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

  private function prepareForTests() {
    Artisan::call('migrate');
    Eloquent::unguard();
    $this->seedTestData();
    Eloquent::reguard();
    Mail::pretend(true);
  }

  private function seedTestData() {
    Users::create([
      'name'              => 'John Doe',
      'role'              => 'manager',
      'email'             => 'jdoe@gmail.com',
      'phone'             => '555-555-5555',
      'password'          => Hash::make('ManagerPassword'),
      'remember_token'    => str_random(50)
    ]);
    Users::create([
      'name'              => 'Jane Doe',
      'role'              => 'manager',
      'email'             => 'janedoe@gmail.com',
      'phone'             => '555-555-6666',
      'password'          => Hash::make('ManagerPassword'),
      'remember_token'    => str_random(50)
    ]);
    Users::create([
      'name'              => 'Alfred E. Neuman',
      'role'              => 'employee',
      'email'             => 'aenewman@gmail.com',
      'phone'             => '555-555-7777',
      'password'          => Hash::make('EmployeePassword'),
      'remember_token'    => str_random(50)
    ]);
    Users::create([
      'name'              => 'Jenny',
      'role'              => 'employee',
      'email'             => 'jenny5309@gmail.com',
      'phone'             => '614-867-5307',
      'password'          => Hash::make('EmployeePassword'),
      'remember_token'    => str_random(50)
    ]);

    Shifts::create([
      'start_time'    => '2016-08-01 10:00:00',
      'end_time'      => '2016-08-01 16:00:00',
      'manager_id'    => 1,
      'employee_id'   => 3,
      'break'         => 0.5
    ]);
    Shifts::create([
      'start_time'    => '2016-08-01 16:00:00',
      'end_time'      => '2016-08-01 23:30:00',
      'manager_id'    => 2,
      'employee_id'   => 4,
      'break'         => 0.5
    ]);
    Shifts::create([
      'start_time'    => '2016-08-02 10:00:00',
      'end_time'      => '2016-08-02 16:00:00',
      'manager_id'    => 2,
      'employee_id'   => 3,
      'break'         => 0.5
    ]);
    Shifts::create([
      'start_time'    => '2016-08-02 16:00:00',
      'end_time'      => '2016-08-02 23:30:00',
      'manager_id'    => 1,
      'employee_id'   => 4,
      'break'         => 0.5
    ]);

  }

}
