<?php

class UsersApiEndpointTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->instance = new UsersController();
  }

  public function testGetAllUserData_ReturnsStatusOK() {
    $response = $this->call('GET', 'v1.1/users');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testGetAllUserData_ReturnsArrayOfUsers() {
    $response = $this->call('GET', 'v1.1/users');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertObjectHasAttribute('users', $data);
    $this->assertInternalType('array', $data->users);
  }

  public function testGetAllUserData_ReturnsValidUserData() {
    $response = $this->call('GET', 'v1.1/users');
    $this->assertResponseOk();

    $data = json_decode($response->getContent());

    $this->assertGreaterThan(0, count($data->users));

    $this->assertEquals('John Doe', $data->users[0]->name);
    $this->assertEquals('manager', $data->users[0]->role);
    $this->assertEquals('jdoe@gmail.com', $data->users[0]->email);
    $this->assertEquals('555-555-5555', $data->users[0]->phone);
  }


  public function testCreateUser_ReturnsStatusOK() {
    $input = [
      'name'      => 'Marcus T. Testalot',
      'role'      => 'employee',
      'email'     => 'mtestalot@gmail.com',
      'phone'     => '614-859-9559',
      'password'  => Hash::make('EmployeePassword')
    ];

    $response = $this->call('POST', 'v1.1/users', $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testCreateUser_DoesCreateUserRecord() {
    $input = [
      'name'      => 'Vivian J. Testalot',
      'role'      => 'manager',
      'email'     => 'vtestalot@gmail.com',
      'phone'     => '614-436-4180',
      'password'  => Hash::make('ManagerPassword')
    ];

    $response = $this->call('POST', 'v1.1/users', $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
    $this->assertObjectHasAttribute('user', $data);
    $this->assertObjectHasAttribute('id', $data->user);
    $user = Users::find($data->user->id);

    $this->assertEquals($input['name'],  $user->name);
    $this->assertEquals($input['role'],  $user->role);
    $this->assertEquals($input['email'], $user->email);
    $this->assertEquals($input['phone'], $user->phone);

  }

  public function testUpdateUser_ReturnsStatusOK() {
    $input = [
      'name'      => 'Alfred E Newman',
      'role'      => 'employee',
      'email'     => 'anewman@gmail.com',
      'phone'     => '614-859-9559',
      'password'  => Hash::make('EmployeeTest')
    ];

    $response = $this->call('PUT', "v1.1/users/3", $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testUpdateUser_DoesUpdateUserRecord() {
    $input = [
      'name'      => 'Alfred X Newman',
      'role'      => 'employee',
      'email'     => 'axnewman@gmail.com',
      'phone'     => '614-515-2434',
      'password'  => Hash::make('EmployeeTest')
    ];

    $response = $this->call('PUT', "v1.1/users/3", $input);
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
    $this->assertObjectHasAttribute('user', $data);
    $this->assertObjectHasAttribute('id', $data->user);
    $user = Users::find(3);

    $this->assertEquals($input['name'],  $user->name);
    $this->assertEquals($input['role'],  $user->role);
    $this->assertEquals($input['email'], $user->email);
    $this->assertEquals($input['phone'], $user->phone);
  }

  public function testGetSingleUser_ReturnsStatusOK() {
    $response = $this->call('GET', "v1.1/users/3");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testGetSingleUser_ReturnsUser() {
    $response = $this->call('GET', "v1.1/users/3");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());

    $this->assertObjectHasAttribute('user',   $data);
    $this->assertEquals('Alfred E. Neuman',   $data->user->name);
    $this->assertEquals('employee',           $data->user->role);
    $this->assertEquals('aenewman@gmail.com', $data->user->email);
    $this->assertEquals('555-555-7777',       $data->user->phone);
  }

  public function testDeleteSingleUser_ReturnsStatusOK() {
    $response = $this->call('DELETE', "v1.1/users/3");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $this->assertEquals($data->status, "ok");
  }

  public function testDeleteSingleUser_DoesDeleteUser() {
    $data = Users::find(3);
    $this->assertNotNull($data);

    $response = $this->call('DELETE', "v1.1/users/3");
    $this->assertResponseOk();

    $data = json_decode($response->getContent());
    $data = Users::find(3);
    $this->assertNull($data);
  }
}
