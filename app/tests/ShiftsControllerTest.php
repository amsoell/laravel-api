<?php

class ShiftsControllerTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->instance = new ShiftsController();
  }

  public function testIndex_ReturnsArrayOfShifts() {
    $response = json_decode($this->instance->index());

    $this->assertObjectHasAttribute('shifts', $response);
    $this->assertInternalType('array', $response->shifts);
    $this->assertGreaterThan(0, count($response->shifts));
    $this->assertEquals('2016-08-01 16:00:00',  $response->shifts[1]->start_time);
    $this->assertEquals('2016-08-01 23:30:00',  $response->shifts[1]->end_time);
    $this->assertEquals(2,                      $response->shifts[1]->manager_id);
    $this->assertEquals(4,                      $response->shifts[1]->employee_id);
    $this->assertEquals(0.5,                    $response->shifts[1]->break);
  }

  public function testStore_DoesCreateShiftRecord() {
    $input = [
      'start_time'    => '2016-10-01 16:00:00',
      'end_time'      => '2016-10-01 23:30:00',
      'manager_id'    => 2,
      'employee_id'   => 4,
      'break'         => 1.0
    ];

    Input::replace($input);
    $response = json_decode($this->instance->store());

    $this->assertEquals($response->status, "ok");
    $this->assertObjectHasAttribute('shift', $response);
    $this->assertObjectHasAttribute('id', $response->shift);
    $shift = Shifts::find($response->shift->id);

    $this->assertEquals($input['start_time'],   $shift->start_time);
    $this->assertEquals($input['end_time'],     $shift->end_time);
    $this->assertEquals($input['manager_id'],   $shift->manager_id);
    $this->assertEquals($input['employee_id'],  $shift->employee_id);
    $this->assertEquals($input['break'],        $shift->break);
  }


  public function testUpdate_DoesUpdateShiftRecord() {
    $input = [
      'start_time'    => '2016-08-03 11:00:00',
      'end_time'      => '2016-08-03 17:00:00',
      'employee_id'   => 1,
      'manager_id'    => 4,
      'break'         => 1.5
    ];

    Input::replace($input);
    $response = json_decode($this->instance->update(3));

    $this->assertEquals($response->status, "ok");
    $this->assertObjectHasAttribute('shift', $response);
    $this->assertObjectHasAttribute('id', $response->shift);
    $shift = Shifts::find(3);

    $this->assertEquals($input['start_time'],   $shift->start_time);
    $this->assertEquals($input['end_time'],     $shift->end_time);
    $this->assertEquals($input['employee_id'],  $shift->employee_id);
    $this->assertEquals($input['manager_id'],   $shift->manager_id);
    $this->assertEquals($input['break'],        $shift->break);
  }

  public function testShow_ReturnsShift() {
    $response = json_decode($this->instance->show(2));

    $this->assertObjectHasAttribute('shift',    $response);
    $this->assertEquals('2016-08-01 16:00:00',  $response->shift->start_time);
    $this->assertEquals('2016-08-01 23:30:00',  $response->shift->end_time);
    $this->assertEquals(2,                      $response->shift->manager_id);
    $this->assertEquals(4,                      $response->shift->employee_id);
    $this->assertEquals(0.5,                    $response->shift->break);
  }

  public function testDestroy_DoesDeleteShift() {
    $data = Shifts::find(1);
    $this->assertNotNull($data);

    $response = $this->instance->destroy(1);

    $data = Shifts::find(1);
    $this->assertNull($data);
  }
}
