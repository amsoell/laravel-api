<?php

class UsersControllerTest extends TestCase {

  public function setUp() {
    parent::setUp();
    $this->instance = new UsersController();
  }

  public function testIndex_ReturnsArrayOfUsers() {
    $response = json_decode($this->instance->index());

    $this->assertEquals('ok', $response->status);
    $this->assertInternalType('array', $response->users);
    $this->assertGreaterThan(0, $response->users);
    $this->assertEquals('John Doe',       $response->users[0]->name);
    $this->assertEquals('manager',        $response->users[0]->role);
    $this->assertEquals('jdoe@gmail.com', $response->users[0]->email);
    $this->assertEquals('555-555-5555',   $response->users[0]->phone);
  }

  public function testStore_DoesCreateUserRecord() {
    $input = [
      'name'      => 'Vivian J. Testalot',
      'role'      => 'manager',
      'email'     => 'vtestalot@gmail.com',
      'phone'     => '614-436-4180',
      'password'  => Hash::make('ManagerPassword')
    ];

    Input::replace($input);
    $response = $this->instance->store();

    $data = json_decode($response);
    $this->assertEquals($data->status, "ok");
    $this->assertObjectHasAttribute('user', $data);
    $this->assertObjectHasAttribute('id', $data->user);

    $user = Users::find($data->user->id);
    $this->assertEquals($input['name'],  $user->name);
    $this->assertEquals($input['role'],  $user->role);
    $this->assertEquals($input['email'], $user->email);
    $this->assertEquals($input['phone'], $user->phone);
  }

  public function testUpdate_DoesUpdateUserRecord() {
    $input = [
      'name'      => 'Alfred X Newman',
      'role'      => 'employee',
      'email'     => 'axnewman@gmail.com',
      'phone'     => '614-515-2434',
      'password'  => Hash::make('EmployeeTest')
    ];

    Input::replace($input);
    $response = json_decode($this->instance->update(3));

    $this->assertEquals($response->status, "ok");
    $this->assertObjectHasAttribute('user', $response);
    $this->assertObjectHasAttribute('id',   $response->user);

    $user = Users::find(3);
    $this->assertEquals($input['name'],  $user->name);
    $this->assertEquals($input['role'],  $user->role);
    $this->assertEquals($input['email'], $user->email);
    $this->assertEquals($input['phone'], $user->phone);
  }

  public function testShow_ReturnsUser() {
    $response = json_decode($this->instance->show(3));

    $this->assertObjectHasAttribute('user',   $response);
    $this->assertEquals('Alfred E. Neuman',   $response->user->name);
    $this->assertEquals('employee',           $response->user->role);
    $this->assertEquals('aenewman@gmail.com', $response->user->email);
    $this->assertEquals('555-555-7777',       $response->user->phone);
  }

  public function testDestroy_DoesDeleteUser() {
    $data = Users::find(3);
    $this->assertNotNull($data);

    $this->instance->destroy(3);

    $data = Users::find(3);
    $this->assertNull($data);
  }

}
