<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>API User Stories</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		html body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:left;
			color: #999;
			margin: 10px 40px;
		}

		body hr {
  		margin: 2em auto 3em;
		}

		.complete {
  		color: #009900;
  		text-transform: uppercase;
		}

		.incomplete {
  		color: #990000;
  		text-transform: uppercase;
		}
	</style>
	<link rel="stylesheet" href="css/cardinal.min.css" type="text/css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body>
  <h1>User Stories</h1>
  <h2>API Testing Suite</h2>
  <hr />
  <h3>Story #1: As an employee, I want to know when I am working, by being able to see all of the shifts assigned to me. <span class="complete">complete</span></h3>
  <form action="v1/getSchedule" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #2: As an employee, I want to know who I am working with, by being able see the employees that are working during the same time period as me. <span class="complete">complete</span></h3>
  <form action="v1/getOverlappingShifts" method="GET">
    <label for="shift_id">Shift ID</label> <input id="shift_id" type="tel" name="shift_id" value="23" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #3: As an employee, I want to know how much I worked, by being able to get a summary of hours worked for each week. <span class="complete">complete</span></h3>
  <form action="v1/getEmployeeHoursWorked" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #4: As an employee, I want to be able to contact my managers, by seeing manager contact information for my shifts. <span class="complete">complete</span></h3>
  <form action="v1/getSchedule" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <aside>This is the same API call as in User Story #1, they're both essentially the same story: An employee wants information about their upcoming shifts. No reason not to give them the manager information as part of that call.</aside>
  <hr />

  <h3>Story #5: As a manager, I want to schedule my employees, by creating shifts for any employee. <span class="complete">complete</span></h3>
  <form action="v1/createShift" method="POST">
    <label for="start_time">Start Time</label> <input id="start_time" type="datetime" name="start_time" placeholder="2015-07-01 08:00:00" /><br />
    <label for="end_time">End Time</label> <input id="end_time" type="datetime" name="end_time" placeholder="2015-07-01 14:00:00" /><br />
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <label for="manager_id">Manager</label> <input id="manager_id" type="tel" name="manager_id" placeholder="1" /><br />
    <label for="break">Break</label> <input id="break" type="tel" name="break" placeholder="0.0" /><br />
    <input type="submit" value="Create" />
  </form>
  <hr />

  <h3>Story #6: As a manager, I want to see the schedule, by listing shifts within a specific time period. <span class="complete">complete</span></h3>
  <form action="v1/getUpcomingShifts" method="GET">
    <label for="start_time">Begin date</label> <input id="start_time" type="datetime" name="start_time" placeholder="2015-06-01" /><br />
    <label for="end_time">End date</label> <input id="end_time" type="datetime" name="end_time" placeholder="2015-07-01" /><br />
    <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #7: As a manager, I want to be able to change a shift, by updating the time details. <span class="complete">complete</span></h3>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#story7_submit').on('click', function() {

        $storyData          = $(this).parents('form');
        var payload         = new Object();
        payload.shift_id    = $storyData.find('#shift_id').val();
        payload.start_time  = $storyData.find('#start_time').val();
        payload.end_time    = $storyData.find('#end_time').val();

        $.ajax({
          url: 'v1/updateShift',
          method: 'PUT',
          dataType: 'text',
          data: payload,
          success: function(data, status, xhr) {
            $('#story7_output').html(data);
          },
          error: function(xhr, status, error) {
            $('#story7_output').html(error);
          }
        })
      });
    });
  </script>
  <form action="v1/updateShift" method="PUT">
    <label for="shift_id">Shift</label> <input id="shift_id" type="tel" name="shift_id" placeholder="1" /><br />
    <label for="start_time">Start time</label> <input id="start_time" type="datetime" name="start_time" value="2015-06-10 08:00:00" /><br />
    <label for="end_time">End time</label> <input id="end_time" type="datetime" name="end_time" value="2015-06-10 12:00:00" /><br />
    <input id="story7_submit" type="button" value="Update" />
  </form>
  <div id="story7_output"></div>
  <hr />

  <h3>Story #8: As a manager, I want to be able to assign a shift, by changing the employee that will work a shift. <span class="complete">complete</span></h3>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#story8_submit').on('click', function() {

        $storyData          = $(this).parents('form');
        var payload         = new Object();
        payload.shift_id    = $storyData.find('#shift_id').val();
        payload.employee_id = $storyData.find('#employee_id').val();

        $.ajax({
          url: 'v1/assignShift',
          method: 'PUT',
          dataType: 'text',
          data: payload,
          success: function(data, status, xhr) {
            $('#story8_output').html(data);
          },
          error: function(xhr, status, error) {
            $('#story8_output').html(error);
          }
        })
      });
    });
  </script>
  <form action="v1/assignShift" method="PUT">
    <label for="shift_id">Shift</label> <input id="shift_id" type="tel" name="shift_id" placeholder="1" /><br />
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <input id="story8_submit" type="button" value="Update" />
  </form>
  <div id="story8_output"></div>
  <hr />

  <h3>Story #9: As a manager, I want to contact an employee, by seeing employee details. <span class="complete">complete</span></h3>
  <form action="v1/getEmployee" method="GET">
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Extra: Create user</h3>
  <form action="v1/createUser" method="POST">
    <label for="name">Name</label> <input id="name" type="tel" name="name" placeholder="John Doe" /><br />
    <label for="name">Email</label> <input id="email" type="email" name="email" placeholder="jdoe@gmail.com" /><br />
    <label for="phone">Phone</label> <input id="phone" type="phone" name="phone" placeholder="614-316-2766" /><br />
    <label for="password">Password</label> <input id="password" type="password" name="password" placeholder="supersecret" /><br />

    <label for="role">Role</label> <select id="role" name="role"><option></option><option value="manager">Manager</option><option value="employee">Employee</option></select><br />
    <input type="submit" value="Create" />
  </form>
</body>
</html>
