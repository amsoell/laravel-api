<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>API User Stories</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		html body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:left;
			color: #999;
			margin: 10px 40px;
		}

		body hr {
  		margin: 2em auto 3em;
		}

		.complete {
  		color: #009900;
  		text-transform: uppercase;
		}

		.incomplete {
  		color: #990000;
  		text-transform: uppercase;
		}
	</style>
	<link rel="stylesheet" href="css/cardinal.min.css" type="text/css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body>
  <h1>User Stories</h1>
  <h2>Basic API Tests</h2>
  <hr />
  <h3>Shift Creation</h3>
    <form action="v1.1/shifts" method="POST">
      <label for="shift_create_start_time">Start Time</label> <input id="shift_create_start_time" type="datetime" name="start_time" placeholder="2015-07-01 08:00:00" /><br />
      <label for="shift_create_end_time">End Time</label> <input id="shift_create_end_time" type="datetime" name="end_time" placeholder="2015-07-01 14:00:00" /><br />
      <label for="shift_create_employee_id">Employee</label> <input id="shift_create_employee_id" type="tel" name="employee_id" placeholder="5" /><br />
      <label for="shift_create_manager_id">Manager</label> <input id="shift_create_manager_id" type="tel" name="manager_id" placeholder="1" /><br />
      <label for="shift_create_break">Break</label> <input id="shift_create_break" type="tel" name="break" placeholder="0.0" /><br />
      <input id="shift_create_submit" type="submit" value="Create" />
    </form>
  <hr />
  <h3>Shift Read</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#shift_read_submit').on('click', function() {
          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#shift_read_id').val();
          window.location.href = 'v1.1/shifts/'+payload.id
        });
      });
    </script>

    <form action="v1.1/shifts" method="GET">
      <label for="shift_read_id">Shift</label> <input id="shift_read_id" type="tel" name="id" placeholder="1" /><br />
      <input id="shift_read_submit" type="button" value="Get" />
    </form>
  <hr />
  <h3>Shift Update</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#shift_update_submit').on('click', function() {

          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#shift_update_id').val();
          payload.employee_id = $data.find('#shift_update_employee_id').val();
          payload.manager_id  = $data.find('#shift_update_manager_id').val();
          payload.start_time  = $data.find('#shift_update_start_time').val();
          payload.end_time    = $data.find('#shift_update_end_time').val();
          payload.break       = $data.find('#shift_update_break').val();

          $.ajax({
            url: 'v1.1/shifts/'+payload.id,
            method: 'PUT',
            data: payload,
            success: function(data, status, xhr) {
              $('#shift_update_output').html(data);
            },
            error: function(xhr, status, error) {
              $('#shift_update_output').html(error);
            }
          })
        });
      });
    </script>
    <form action="v1.1/shifts" method="PUT">
      <label for="shift_update_id">Shift</label> <input id="shift_update_id" type="tel" name="id" placeholder="1" /><br />
      <label for="shift_update_start_time">Start Time</label> <input id="shift_update_start_time" type="datetime" name="start_time" placeholder="2015-07-01 08:00:00" /><br />
      <label for="shift_update_end_time">End Time</label> <input id="shift_update_end_time" type="datetime" name="end_time" placeholder="2015-07-01 14:00:00" /><br />
      <label for="shift_update_employee_id">Employee</label> <input id="shift_update_employee_id" type="tel" name="employee_id" placeholder="5" /><br />
      <label for="shift_update_manager_id">Manager</label> <input id="shift_update_manager_id" type="tel" name="manager_id" placeholder="1" /><br />
      <label for="shift_update_break">Break</label> <input id="shift_update_break" type="tel" name="break" placeholder="0.0" /><br />
      <input id="shift_update_submit" type="button" value="Update" />
    </form>
    <div id="shift_update_output"></div>
  <hr />
  <h3>Shift Delete</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#shift_delete_submit').on('click', function() {

          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#shift_delete_id').val();

          $.ajax({
            url: 'v1.1/shifts/'+payload.id,
            method: 'DELETE',
            success: function(data, status, xhr) {
              $('#shift_delete_output').html(data);
            },
            error: function(xhr, status, error) {
              $('#shift_delete_output').html(error);
            }
          })
        });
      });
    </script>
    <form action="v1.1/shifts" method="DELETE">
      <label for="shift_delete_id">Shift</label> <input id="shift_delete_id" type="tel" name="id" placeholder="1" /><br />
      <input id="shift_delete_submit" type="button" value="Delete" />
    </form>
    <div id="shift_delete_output"></div>
  <hr />

  <h3>User Creation</h3>
    <form action="v1.1/users" method="POST">
      <label for="user_create_name">Name</label> <input id="user_create_name" type="text" name="name" placeholder="John Q. Public" /><br />
      <label for="user_create_role">Role</label> <select id="user_create_role" name="role"><option value=""></option><option value="employee">Employee</option><option value="manager">Manager</option></select><br />
      <label for="user_create_email">Email</label> <input id="user_create_email" type="email" name="email" placeholder="jqpublic@gmail.com" /><br />
      <label for="user_create_phone">Phone</label> <input id="user_create_phone" type="tel" name="phone" placeholder="555-867-5309" /><br />
      <label for="user_create_password">Password</label> <input id="user_create_password" type="text" name="password" placeholder="" /><br />
      <input id="user_create_submit" type="submit" value="Create" />
    </form>
  <hr />
  <h3>User Read</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#user_read_submit').on('click', function() {
          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#user_read_id').val();
          window.location.href = 'v1.1/users/'+payload.id
        });
      });
    </script>

    <form action="v1.1/users" method="GET">
      <label for="user_read_id">User</label> <input id="user_read_id" type="tel" name="id" placeholder="1" /><br />
      <input id="user_read_submit" type="button" value="Get" />
    </form>
  <hr />
  <h3>User Update</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#user_update_submit').on('click', function() {

          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#user_update_id').val();
          payload.name        = $data.find('#user_update_name').val();
          payload.role        = $data.find('#user_update_role').val();
          payload.email       = $data.find('#user_update_email').val();
          payload.phone       = $data.find('#user_update_phone').val();
          payload.password    = $data.find('#user_update_password').val();


          $.ajax({
            url: 'v1.1/users/'+payload.id,
            method: 'PUT',
            data: payload,
            success: function(data, status, xhr) {
              $('#user_update_output').html(data);
            },
            error: function(xhr, status, error) {
              $('#user_update_output').html(error);
            }
          })
        });
      });
    </script>
    <form action="v1.1/users" method="PUT">
      <label for="user_update_id">User</label> <input id="user_update_id" type="tel" name="id" placeholder="1" /><br />
      <label for="user_update_name">Name</label> <input id="user_update_name" type="text" name="name" placeholder="John Q. Public" /><br />
      <label for="user_update_role">Role</label> <select id="user_update_role" name="role"><option value=""></option><option value="employee">Employee</option><option value="manager">Manager</option></select><br />
      <label for="user_update_email">Email</label> <input id="user_update_email" type="email" name="email" placeholder="jqpublic@gmail.com" /><br />
      <label for="user_update_phone">Phone</label> <input id="user_update_phone" type="tel" name="phone" placeholder="555-867-5309" /><br />
      <label for="user_update_password">Password</label> <input id="user_update_password" type="text" name="password" placeholder="" /><br />
      <input id="user_update_submit" type="button" value="Update" />
    </form>
    <div id="user_update_output"></div>
  <hr />
  <h3>User Delete</h3>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#user_delete_submit').on('click', function() {

          $data               = $(this).parents('form');
          var payload         = new Object();
          payload.id          = $data.find('#user_delete_id').val();

          $.ajax({
            url: 'v1.1/users/'+payload.id,
            method: 'DELETE',
            success: function(data, status, xhr) {
              $('#user_delete_output').html(data);
            },
            error: function(xhr, status, error) {
              $('#user_delete_output').html(error);
            }
          })
        });
      });
    </script>
    <form action="v1.1/users" method="DELETE">
      <label for="user_delete_id">User</label> <input id="user_delete_id" type="tel" name="id" placeholder="1" /><br />
      <input id="user_delete_submit" type="button" value="Delete" />
    </form>
    <div id="user_delete_output"></div>
  <hr />


  <h2>API Testing Suite</h2>
  <hr />
  <h3>Story #1: As an employee, I want to know when I am working, by being able to see all of the shifts assigned to me. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getSchedule" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #2: As an employee, I want to know who I am working with, by being able see the employees that are working during the same time period as me. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getOverlappingShifts" method="GET">
    <label for="shift_id">Shift ID</label> <input id="shift_id" type="tel" name="shift_id" value="23" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #3: As an employee, I want to know how much I worked, by being able to get a summary of hours worked for each week. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getEmployeeHoursWorked" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #4: As an employee, I want to be able to contact my managers, by seeing manager contact information for my shifts. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getSchedule" method="GET">
    <label for="employee_id">Employee ID</label> <input id="employee_id" type="tel" name="employee_id" value="5" /> <input type="submit" value="Lookup" />
  </form>
  <aside>This is the same API call as in User Story #1, they're both essentially the same story: An employee wants information about their upcoming shifts. No reason not to give them the manager information as part of that call.</aside>
  <hr />

  <h3>Story #5: As a manager, I want to schedule my employees, by creating shifts for any employee. <span class="incomplete">incomplete</span></h3>
  <form action="v1/createShift" method="POST">
    <label for="start_time">Start Time</label> <input id="start_time" type="datetime" name="start_time" placeholder="2015-07-01 08:00:00" /><br />
    <label for="end_time">End Time</label> <input id="end_time" type="datetime" name="end_time" placeholder="2015-07-01 14:00:00" /><br />
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <label for="manager_id">Manager</label> <input id="manager_id" type="tel" name="manager_id" placeholder="1" /><br />
    <label for="break">Break</label> <input id="break" type="tel" name="break" placeholder="0.0" /><br />
    <input type="submit" value="Create" />
  </form>
  <hr />

  <h3>Story #6: As a manager, I want to see the schedule, by listing shifts within a specific time period. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getUpcomingShifts" method="GET">
    <label for="start_time">Begin date</label> <input id="start_time" type="datetime" name="start_time" placeholder="2015-06-01" /><br />
    <label for="end_time">End date</label> <input id="end_time" type="datetime" name="end_time" placeholder="2015-07-01" /><br />
    <input type="submit" value="Lookup" />
  </form>
  <hr />

  <h3>Story #7: As a manager, I want to be able to change a shift, by updating the time details. <span class="incomplete">incomplete</span></h3>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#story7_submit').on('click', function() {

        $storyData          = $(this).parents('form');
        var payload         = new Object();
        payload.shift_id    = $storyData.find('#shift_id').val();
        payload.start_time  = $storyData.find('#start_time').val();
        payload.end_time    = $storyData.find('#end_time').val();

        $.ajax({
          url: 'v1/updateShift',
          method: 'PUT',
          dataType: 'text',
          data: payload,
          success: function(data, status, xhr) {
            $('#story7_output').html(data);
          },
          error: function(xhr, status, error) {
            $('#story7_output').html(error);
          }
        })
      });
    });
  </script>
  <form action="v1/updateShift" method="PUT">
    <label for="shift_id">Shift</label> <input id="shift_id" type="tel" name="shift_id" placeholder="1" /><br />
    <label for="start_time">Start time</label> <input id="start_time" type="datetime" name="start_time" value="2015-06-10 08:00:00" /><br />
    <label for="end_time">End time</label> <input id="end_time" type="datetime" name="end_time" value="2015-06-10 12:00:00" /><br />
    <input id="story7_submit" type="button" value="Update" />
  </form>
  <div id="story7_output"></div>
  <hr />

  <h3>Story #8: As a manager, I want to be able to assign a shift, by changing the employee that will work a shift. <span class="incomplete">incomplete</span></h3>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#story8_submit').on('click', function() {

        $storyData          = $(this).parents('form');
        var payload         = new Object();
        payload.shift_id    = $storyData.find('#shift_id').val();
        payload.employee_id = $storyData.find('#employee_id').val();

        $.ajax({
          url: 'v1/assignShift',
          method: 'PUT',
          dataType: 'text',
          data: payload,
          success: function(data, status, xhr) {
            $('#story8_output').html(data);
          },
          error: function(xhr, status, error) {
            $('#story8_output').html(error);
          }
        })
      });
    });
  </script>
  <form action="v1/assignShift" method="PUT">
    <label for="shift_id">Shift</label> <input id="shift_id" type="tel" name="shift_id" placeholder="1" /><br />
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <input id="story8_submit" type="button" value="Update" />
  </form>
  <div id="story8_output"></div>
  <hr />

  <h3>Story #9: As a manager, I want to contact an employee, by seeing employee details. <span class="incomplete">incomplete</span></h3>
  <form action="v1/getEmployee" method="GET">
    <label for="employee_id">Employee</label> <input id="employee_id" type="tel" name="employee_id" placeholder="5" /><br />
    <input type="submit" value="Lookup" />
  </form>
</body>
</html>
