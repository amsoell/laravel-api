<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShifts extends Migration {

	public function up() {
		Schema::create('shifts', function($shiftsTable) {
  		$shiftsTable->increments('id');
  		$shiftsTable->integer('manager_id')->references('id')->on('users');
  		$shiftsTable->integer('employee_id')->references('id')->on('users');
  		$shiftsTable->float('break');
  		$shiftsTable->dateTime('start_time');
  		$shiftsTable->dateTime('end_time');
  		$shiftsTable->timestamps();
		});
	}

	public function down() {
		Schema::drop('shifts');
	}

}
