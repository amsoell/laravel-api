<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($usersTable) {
  		$usersTable->increments('id');
  		$usersTable->string('name', 100);
  		$usersTable->enum('role', ['employee','manager']);
  		$usersTable->string('email', 100)->unique();
  		$usersTable->string('phone', 20);
  		$usersTable->string('password', 100);
  		$usersTable->rememberToken();
  		$usersTable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
