<?php

  class ShiftsTableSeeder Extends Seeder {
    public function run() {
      $faker = Faker\Factory::create();

      Shifts::truncate();
      $shift_length = new DateInterval('PT28800S');

      for ($i=0; $i<10; $i++) {
        $shift_start_time = $faker->dateTimeThisYear();
        $shift_end_time = date_add($shift_start_time, $shift_length);
        $shift = Shifts::create(array(
          'manager_id'    => rand(1,4),
          'employee_id'   => rand(5,24),
          'break'         => rand(1,5),
          'start_time'    => $shift_start_time->format('Y-m-d H:i:s'),
          'end_time'      => $shift_end_time->format('Y-m-d H:i:s')
        ));
      }
    }
  }

?>