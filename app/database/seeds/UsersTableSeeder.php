<?php

  class UsersTableSeeder Extends Seeder {
    public function run() {
      $faker = Faker\Factory::create();

      Users::truncate();

      // Seed managers
      for ($i=0; $i<4; $i++) {
        $manager                = Users::create(array(
            'name'              => $faker->name,
            'role'              => 'manager',
            'email'             => $faker->unique()->email,
            'phone'             => $faker->phoneNumber,
            'password'          => Hash::make('ManagerPassword'),
            'remember_token'    => str_random(50)
        ));
      }

      // Seed employees
      for ($i=0; $i<25; $i++) {
        $employee = Users::create(array(
            'name'              => $faker->name,
            'role'              => 'employee',
            'email'             => $faker->unique()->email,
            'phone'             => $faker->phoneNumber,
            'password'          => Hash::make('EmployeePassword'),
            'remember_token'    => str_random(50)
        ));
      }
    }
  }

?>